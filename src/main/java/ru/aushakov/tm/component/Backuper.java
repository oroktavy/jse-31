package ru.aushakov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.aushakov.tm.bootstrap.Bootstrap;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.enumerated.ConfigProperty;

import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backuper implements Runnable {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public Backuper(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private void load() {
        @NotNull final String fileXml = bootstrap.getDataService().getFileBackup();
        @NotNull final File backup = new File(fileXml);
        if (backup.exists()) {
            bootstrap.parseCommand(TerminalConst.DATA_LOAD_BACKUP);
        } else {
            bootstrap.initData();
        }
    }

    public void run() {
        bootstrap.parseCommand(TerminalConst.DATA_SAVE_BACKUP);
    }

    public void init() {
        load();
        start();
    }

    private void start() {
        @NotNull final Integer sleepTime =
                bootstrap.getPropertyService().getIntProperty(ConfigProperty.BACKUP_SLEEP_TIME);
        executorService.scheduleWithFixedDelay(this, 0, sleepTime, TimeUnit.SECONDS);
    }

    private void stop() {
        executorService.shutdown();
    }

}
