package ru.aushakov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.service.ICommandService;
import ru.aushakov.tm.bootstrap.Bootstrap;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.enumerated.ConfigProperty;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileScanner implements Runnable {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void run() {
        @NotNull final String commandsPath = bootstrap.getPropertyService()
                .getProperty(ConfigProperty.FILE_SCANNER_DIR);
        @NotNull final File dir = new File(commandsPath);
        @NotNull final ICommandService commandService = bootstrap.getCommandService();
        @Nullable File[] files = dir.listFiles();
        if (files == null) return;
        files = Arrays.stream(files).filter(Objects::nonNull)
                .sorted(Comparator.comparing(File::lastModified)).toArray(File[]::new);
        for (@Nullable final File item : files) {
            if (item == null || !item.isFile()) continue;
            @NotNull final String fileName = item.getName();
            @Nullable final AbstractCommand command = commandService.getCommandByName(fileName);
            Optional.ofNullable(command).ifPresent(c -> {
                if (c.getArgument() != null) {
                    c.execute();
                    if (!item.delete()) System.out.println("Can not delete file " + fileName);
                }
            });
        }
    }

    public void init() {
        @NotNull final Integer sleepTime =
                bootstrap.getPropertyService().getIntProperty(ConfigProperty.FILE_SCANNER_SLEEP_TIME);
        executorService.scheduleWithFixedDelay(this, 0, sleepTime, TimeUnit.SECONDS);
    }

    private void stop() {
        executorService.shutdown();
    }

}
