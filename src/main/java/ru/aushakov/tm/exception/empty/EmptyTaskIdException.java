package ru.aushakov.tm.exception.empty;

public class EmptyTaskIdException extends RuntimeException {

    public EmptyTaskIdException() {
        super("Provided task id is empty!");
    }

}
