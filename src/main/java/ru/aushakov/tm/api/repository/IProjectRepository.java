package ru.aushakov.tm.api.repository;

import ru.aushakov.tm.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {
}
