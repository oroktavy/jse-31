package ru.aushakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.command.AbstractUserCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.enumerated.Role;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class UserCreateWithRoleCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.USER_CREATE_WITH_ROLE;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create user with role";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[USER CREATE]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL (OPTIONAL):");
        @Nullable final String email = TerminalUtil.nextLine();
        System.out.println("ENTER ROLE " + Arrays.toString(Role.values()) + " :");
        @Nullable final String roleId = TerminalUtil.nextLine();
        serviceLocator.getUserService().add(login, password, email, roleId);
    }

}
