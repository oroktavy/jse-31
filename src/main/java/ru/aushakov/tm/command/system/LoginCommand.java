package ru.aushakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.TerminalConst;

public final class LoginCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.CMD_LOGIN;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Makes you able to login twice";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("OK now, you're fully logged in, but you can try once more");
    }

}
