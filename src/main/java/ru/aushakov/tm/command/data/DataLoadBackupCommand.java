package ru.aushakov.tm.command.data;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.service.IDataService;
import ru.aushakov.tm.command.AbstractDataCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataLoadBackupCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.DATA_LOAD_BACKUP;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load application data from backup xml";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final IDataService dataService = serviceLocator.getDataService();
        @NotNull final String fileXml = dataService.getFileBackup();
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(fileXml)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final AnnotationIntrospector annotationIntrospector =
                new JaxbAnnotationIntrospector(objectMapper.getTypeFactory());
        objectMapper.setAnnotationIntrospector(annotationIntrospector);
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        dataService.setDomain(domain);
        serviceLocator.getAuthService().logout();
    }

}
